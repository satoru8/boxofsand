import os

def clean_filenames(folder_path):
    for filename in os.listdir(folder_path):
        if filename.endswith('.fbx'):
            new_name = filename.replace('_', '').replace('-', '')
            old_path = os.path.join(folder_path, filename)
            new_path = os.path.join(folder_path, new_name)
            os.rename(old_path, new_path)
            print(f"Renamed {filename} to {new_name}")

export_folder = r'C:/Users/katsu/Desktop/New folder'

clean_filenames(export_folder)