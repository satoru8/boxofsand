import bpy

def process_collection(collection):
    for obj in collection.objects:
        if obj.type == 'MESH':
            bpy.context.view_layer.objects.active = obj
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.normals_make_consistent(inside=False)
            bpy.ops.object.mode_set(mode='OBJECT')

    for child_collection in collection.children:
        process_collection(child_collection)

collection_name = 'Jaws'
collection = bpy.data.collections.get(collection_name)

if collection:
    process_collection(collection)
else:
    print(f"Collection '{collection_name}' not found.")
