import bpy
import os

def fix_normals(collection):
    for obj in collection.objects:
        if obj.type == 'MESH':
            bpy.context.view_layer.objects.active = obj
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.normals_make_consistent(inside=False)
            bpy.ops.object.mode_set(mode='OBJECT')

    for child_collection in collection.children:
        fix_normals(child_collection)

def export_collection(collection, basedir):
    for obj in collection.objects:
        if obj.type == 'MESH':
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj

            name = bpy.path.clean_name(obj.name)
            fn = os.path.join(basedir, name)

            bpy.ops.export_scene.fbx(
                filepath=fn + ".fbx", 
                use_selection=True, 
                path_mode='COPY', 
                # use_active_collection=True,
                axis_forward='-Z',
                axis_up='Y',
                apply_scale_options='FBX_SCALE_ALL',
                bake_space_transform=True, 
                object_types={'MESH'},
                use_mesh_modifiers=True,
                add_leaf_bones=False,
                primary_bone_axis='Y',
                secondary_bone_axis='-X',
                global_scale=0.01,
                apply_unit_scale=True,
                bake_anim=False
            )

            obj.select_set(False)
            print(f"Exported: {fn}.fbx")

    for child_collection in collection.children:
        export_collection(child_collection, basedir)

def clean_filenames(folder_path):
    for filename in os.listdir(folder_path):
        if filename.endswith('.fbx'):
            new_name = filename.replace('_', '').replace('-', '')
            old_path = os.path.join(folder_path, filename)
            new_path = os.path.join(folder_path, new_name)
            os.rename(old_path, new_path)
            print(f"Renamed {filename} to {new_name}")

def main():
    collection_name = 'Jaws'
    collection = bpy.data.collections.get(collection_name)

    if not collection:
        raise Exception(f"Collection '{collection_name}' not found")

    basedir = os.path.dirname(bpy.data.filepath)
    if not basedir:
        raise Exception("Blend file is not saved")

    bpy.ops.object.select_all(action='DESELECT')
    fix_normals(collection)
    export_collection(collection, basedir)

    export_folder = r'C:/Users/katsu/Desktop/New folder'
    clean_filenames(export_folder)

main()