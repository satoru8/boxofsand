import bpy
import os

def process_collection(collection, basedir):
    for obj in collection.objects:
        if obj.type == 'MESH':
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj

            name = bpy.path.clean_name(obj.name)
            fn = os.path.join(basedir, name)

            bpy.ops.export_scene.fbx(
                filepath=fn + ".fbx", 
                use_selection=True, 
                path_mode='COPY', 
                use_active_collection=True,
                axis_forward='-Z',
                axis_up='Y',
                apply_scale_options='FBX_SCALE_ALL',
                bake_space_transform=True, 
                object_types={'MESH'},
                use_mesh_modifiers=True,
                add_leaf_bones=False,
                primary_bone_axis='Y',
                secondary_bone_axis='-X',
                global_scale=0.01,
                apply_unit_scale=True,
                bake_anim=False, 

            )

            obj.select_set(False)
            print(f"Exported: {fn}.fbx")

    for child_collection in collection.children:
        process_collection(child_collection, basedir)

collection_name = 'Jaws'
collection = bpy.data.collections.get(collection_name)

if not collection:
    raise Exception(f"Collection '{collection_name}' not found")

basedir = os.path.dirname(bpy.data.filepath)

if not basedir:
    raise Exception("Blend file is not saved")

view_layer = bpy.context.view_layer
obj_active = view_layer.objects.active
bpy.ops.object.select_all(action='DESELECT')
process_collection(collection, basedir)
view_layer.objects.active = obj_active