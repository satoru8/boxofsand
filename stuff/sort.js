const fs = require('fs')

const readFile = (filePath) => {
	try {
		const data = fs.readFileSync(filePath, 'utf8')
		return data.split('\n').filter(Boolean)
	} catch (error) {
		console.error('Error reading the file:', error.message)
		return []
	}
}

const sortNames = (names) => {
	return names.sort()
}

const printSortedNames = (sortedNames) => {
	console.log('Sorted Names:')
	sortedNames.forEach((name) => {
		console.log(name)
	})
}

const saveToFile = (sortedNames, outputPath) => {
	try {
		fs.writeFileSync(outputPath, sortedNames.join('\n'))
		console.log(`Sorted names have been saved to '${outputPath}'`)
	} catch (error) {
		console.error('Error saving to file:', error.message)
	}
}

const inputFile = 'sortmejs.txt'
const outputFile = 'sortedjs.txt'

const names = readFile(inputFile)
const sortedNames = sortNames(names)
printSortedNames(sortedNames)
saveToFile(sortedNames, outputFile)
