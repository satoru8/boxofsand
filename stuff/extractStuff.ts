import * as fs from 'fs'
import * as path from 'path'

const octdatFolder = 'C:/Program Files (x86)/Steam/steamapps/common/Noble Fates/Noble Fates_Data/StreamingAssets/OctDats'
const outputFile = './typesOutput.json'
const minify = true
const showCounts = false

const scanFolder = (folder: string): string[] => {
	let files: string[] = []
	fs.readdirSync(folder).forEach((file) => {
		const fullPath = path.join(folder, file)
		if (fs.statSync(fullPath).isDirectory()) {
			files = files.concat(scanFolder(fullPath))
		} else if (fullPath.endsWith('.octdat')) {
			files.push(fullPath)
		}
	})
	return files
}

const extractTypes = (file: string): string[] => {
	const types: string[] = []
	const fileContent = fs.readFileSync(file, 'utf-8')
	const typeRegex = /type\s+([A-Z]\w*)/g
	let match
	while ((match = typeRegex.exec(fileContent)) !== null) {
		types.push(match[1])
	}
	return types
}

const countTypes = (types: string[]): Map<string, number> => {
	const typeCount = new Map<string, number>()
	types.forEach((type) => {
		typeCount.set(type, (typeCount.get(type) || 0) + 1)
	})
	return typeCount
}

const getTypes = () => {
	const octdatFiles = scanFolder(octdatFolder)
	let allTypes: string[] = []

	octdatFiles.forEach((file) => {
		const types = extractTypes(file)
		allTypes = allTypes.concat(types)
	})

	const typeCountMap = countTypes(allTypes)
	const sortedTypes = Array.from(typeCountMap.keys()).sort()

	let output: Record<string, number> | string[] = {}

	if (showCounts) {
		output = sortedTypes.reduce((acc, type) => {
			acc[type] = typeCountMap.get(type) || 0
			return acc
		}, {} as Record<string, number>)
	} else {
		output = sortedTypes
	}

	fs.writeFileSync(outputFile, JSON.stringify(output, null, minify ? 0 : 2))
	console.log(`Found ${sortedTypes.length} unique types. Types saved to ${outputFile}.`)
}

getTypes()
