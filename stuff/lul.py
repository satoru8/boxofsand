import turtle
import time
import random

screen = turtle.Screen()
screen.setup(width=1.0, height=1.0)
screen.bgcolor("black")
canvas = screen.getcanvas()
root = canvas.winfo_toplevel()
root.overrideredirect(True)

colors = ["yellow", "red", "blue", "green", "purple", "orange"]
turtles = []

for color in colors:
    t = turtle.Turtle()
    t.shape("turtle")
    t.color(color)
    t.penup()
    t.goto(random.randint(-screen.window_width()//2, screen.window_width()//2),
           random.randint(-screen.window_height()//2, screen.window_height()//2))
    turtles.append(t)

start_time = time.time()
while time.time() - start_time < 10:
    for t in turtles:
        t.left(random.randint(-90, 90))
        t.forward(random.randint(10, 50))
    time.sleep(0.1)

screen.bye()
