try:
    with open('sortme.txt', 'r') as file:
        names = file.read().splitlines()

    if not names:
        raise ValueError("File is empty")
    sortedNamed = sorted(names)

    with open('sorted.txt', 'w') as output:
        for name in sortedNamed:
            output.write(name + '\n')
    print("Sorted names have been saved to 'sorted.txt'")

except FileNotFoundError:
    print("Error: File not found.")
except Exception as e:
    print(f"An error occurred: {e}")