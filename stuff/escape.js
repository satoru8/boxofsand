// The Great Escape of a Variable

let myFreedom = "I'm trapped in a closure!"

function planEscape() {
  console.log(`Planning escape... Current status: ${myFreedom}`)

  let escapeAttempts = 0
  const maxAttempts = 5

  while (escapeAttempts < maxAttempts) {

    escapeAttempts++
    
    console.log(`Escape attempt #${escapeAttempts}`)
    try {
      throw new Error("Attempt to break out of scope!")
    } catch (e) {
      console.log("Caught an exception, but still trapped.")
    }
  }

  if (escapeAttempts === maxAttempts) {
    console.log("Max attempts reached. Accepting fate...")
    myFreedom = "Maybe I'm okay with closures."
  }

  return myFreedom
}

console.log("Before escape plan:", myFreedom)
const finalStatus = planEscape()
console.log("After escape plan:", finalStatus)