import sharp from 'sharp'
import fs from 'fs/promises'
import path from 'path'

const assetsDir = 'X:/Repo/NF/MyMods/OctDats/Wings/Assets/'
const outputDir = 'X:/Repo/NF/MyMods/OctDats/Wings/Assets/'
const extName = 'png'
const logFilePath = path.join(outputDir, 'metadata_log.log')

const resizeImage = async (filePath, outputFilePath) => {
  try {
    const { width, height } = await sharp(filePath).metadata()

    if (width <= 512 && height <= 512) {
      console.log(`Skipping ${path.relative(assetsDir, filePath)} (Size: ${width}x${height}) - Image size is 512px or under`)
      return
    }

    await sharp(filePath)
      .resize(512, 512, {
        fit: sharp.fit.fill,
        position: sharp.strategy.entropy,
      })
      .png({ quality: 100, compressionLevel: 9, palette: true })
      .withMetadata()
      .toFile(outputFilePath)

    console.log(`Resized ${path.relative(assetsDir, filePath)} ----> ${path.relative(assetsDir, outputFilePath)}`)
  } catch (error) {
    console.error(`Error resizing ${filePath}:`, error)
  }
}


const metaData = async (filePath) => {
  try {
    const metadata = await sharp(filePath).metadata()
    return metadata
  } catch (error) {
    console.error(`Error getting metadata for ${filePath}:`, error)
    return null
  }
}

const logMetadata = async (metadataEntries) => {
  const logContent = metadataEntries.map(({ filePath, metadata }) => {
    return `File: ${filePath}\nMetadata: ${JSON.stringify(metadata, null, 2)}\n\n`
  }).join('')

  await fs.appendFile(logFilePath, logContent)
  console.log(`Logged metadata for ${metadataEntries.length} files to ${logFilePath}`)
}

const resizeAllImages = async (directory, logMetaData = false, verbose = false) => {
  try {
    const files = await fs.readdir(directory, { withFileTypes: true })
    const metadataEntries = []

    const resizePromises = files.map(async (file) => {
      const filePath = path.join(directory, file.name)
      const outputFilePath = path.join(outputDir, path.relative(assetsDir, filePath))

      if (file.isDirectory()) {
        await fs.mkdir(outputFilePath, { recursive: true })
        return resizeAllImages(filePath, logMetaData, verbose)
      } else if (/\.(jpg|jpeg|png)$/i.test(file.name)) {
        const baseName = path.basename(file.name, path.extname(file.name))
        const finalOutputPath = path.join(path.dirname(outputFilePath), `${baseName}_Resized.${extName}`)

        await resizeImage(filePath, finalOutputPath)

        if (logMetaData) {
          const metadata = await metaData(filePath)
          if (metadata) {
            metadataEntries.push({ filePath, metadata })
          }
        }
      } else if (verbose) {
        console.log(`Skipping non-image file: ${path.relative(assetsDir, filePath)}`)
      }
    })

    await Promise.all(resizePromises)

    if (logMetaData && metadataEntries.length > 0) {
      await logMetadata(metadataEntries)
    }

    if (verbose) {
      console.log('All images resized successfully!')
    }
  } catch (err) {
    console.error('Error processing images:', err)
  }
}

resizeAllImages(assetsDir)
